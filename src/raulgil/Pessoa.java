package raulgil;

import java.util.Date;

public abstract class Pessoa {
    public String nome;
    public Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }
}