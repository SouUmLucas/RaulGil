package raulgil;

import java.util.Date;

public class Calouro extends Pessoa {
    private String cpf;
    
    public Calouro(String cpf, String nome, Date dataNascimento) {
        this.cpf = cpf;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    public String getCpf() {
        return cpf;
    }
}
