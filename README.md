# Raul Gil em Java


Este é um problema em homenagem ao querido prof. Bortot.

O Programa Raul Gil é um tradicional show televisivo que já tem muitos anos de história. Um dos quadros mais famosos é o show de calouros.

Neste quadro, diversos calouros apresentam um número em busca de um prêmio – geralmente a gravação de um CD e um contrato com uma gravadora.

Durante o show, jurados avaliam o desempenho de cada calouro com uma nota e, ao término do programa, os calouros com as 3 maiores notas ganham prêmios.

Escreva um programa orientado a objetos que permite que:

* Sejam cadastrados calouros para um programa (CPF, Nome, Data de Nascimento);
* Sejam cadastrados jurados para um programa (Nome e Data de Nascimento);
* Sejam cadastradas apresentações para os calouros (cada calouro pode fazer uma ou mais apresentações no mesmo programa – guardar título e descrição de cada apresentação);
* Sejam informadas notas para cada jurado e para cada apresentação;

O programa deve então calcular e exibir:
* A média de cada participante;
* A média geral de notas do programa todo (um numero apenas);
* Os 3 participantes ganhadores em ordem.


### É isso aí, moçada!

*Descrição do problema escrita pelo professor Leandro Luque, disponível [aqui] (https://poofatec.wordpress.com/)*
